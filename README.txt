Usage:
------

Wrap image to two elements with specific classes:

  <div class="grayscale-effect">
    <div class="grayscale-effect-target">
      <img src="..." />
    </div>
  </div>

After module init DOM will be the:

  <div class="grayscale-effect grayscale-effect-processed">
    <div class="grayscale-effect-target">
      <img src="..." class="grayscale-effect-original-image" />
      <canvas ... class="grayscale-effect-desaturated-image" />
    </div>
  </div>

