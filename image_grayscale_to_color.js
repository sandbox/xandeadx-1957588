(function ($) {
  Drupal.behaviors.imageGrayscaleToColor = {
    attach: function (context, settings) {
      $('.grayscale-effect:not(.grayscale-effect-processed)', context).each(function() {
        var $wrapper = $(this);
        var $target = $wrapper.find('.grayscale-effect-target');
        var $img = $target.find('img');
        var $desaturatedImg = $img.clone().appendTo($target);
        
        $img.addClass('grayscale-effect-original-image');
        $desaturatedImg.addClass('grayscale-effect-desaturated-image');
        
        Pixastic.process($desaturatedImg.get(0), 'desaturate', null, function() {
          $wrapper.addClass('grayscale-effect-processed');
        });
      });
    }
  };
})(jQuery);